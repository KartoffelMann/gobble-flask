# Gobbler App

Uses Flask, AWS DynamoDB, and AWS Lambdas

Takes messages and names, then displays them.

Tweaked from this tutorial https://hackernoon.com/using-aws-dynamodb-with-flask-9086c541e001 

# Installation Instructions
Requires Python and AWS CLI. The DynamoDB table can be configured with table.json

Configure your IAM credentials using 
>aws configure

Assign app.py in your environment variables
>export FLASK_APP=app.py

Make a virtual environment
>python3 -m venv venv

Enter the environment then install Flask and boto3
>source venv/bin/activate

>pip install flask boto3

Then run Flask
>flask run --host=0.0.0.0
