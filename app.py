from flask import Flask, jsonify, request, render_template, url_for, redirect
import aws_controller

app = Flask(__name__)


@app.route('/')
def index():
    data = aws_controller.get_items()['Items']
    return render_template('index.html', data=data)
    

@app.route('/get-items')
def get_items():
    return jsonify(aws_controller.get_items())

@app.route('/add-item', methods=['POST'])
def add_item():
    data = request.form
    name = data['name']
    message = data['message']
    aws_controller.add_item(message, name)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run()