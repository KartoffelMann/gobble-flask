import boto3
import hashlib
from random import randint
from datetime import datetime, timezone

epoch = datetime.utcfromtimestamp(0)

dynamoClient = boto3.client('dynamodb')
tableName = 'Gobblerr'


def get_items():
    return dynamoClient.scan(
        TableName=tableName
    )

def add_item(message, name):
    return dynamoClient.put_item(
        TableName=tableName,
        Item={
            
            'MessageID': {
                'N': str(abs(hash(message) * randint(0, 100) ))
            },

            'Likes': {
                'N': '0'
            },

            'Shares': {
                'N': '0'
            },

            'Timestamp': {
                'N': str(int(datetime.utcnow().timestamp()))
            },

            
            'Message': {
                'S': message,
                
            },
            
            'AccountName': {
                'S': name
            },
            
            'PersonaName': {
                'S': name
            }
        }
    )